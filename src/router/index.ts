import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "../views/Home.vue";
import Dashboard from "../views/Dashboard.vue";
import ProducerItem from "@/components/producer-item/producer-item-container.vue";
import Login from "@/components/login/login.vue";
import Register from "@/components/login/register.vue";
import RecipesList from "@/components/recipes/recipes-list.vue";
import ForgotPassword from "@/components/login/forgot-password.vue";
import firebase from "firebase";
import store from "@/store";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/share",
    name: "Share",
    component: () =>
      import(
        /* webpackChunkName: "about" */ "../components/shared-item/share-item-list.vue"
      ),
  },
  {
    path: "/shared-item/:id",
    name: "Shared Item",
    component: () =>
      import(
        /* webpackChunkName: "about" */ "../components/shared-item/shared-item.vue"
      ),
  },
  {
    path: "/notifications",
    name: "Notifications",
    component: () =>
      import(
        /* webpackChunkName: "about" */ "../components/messaging/list-messages.vue"
      ),
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
  {
    path: "/producer/:name/:id",
    component: ProducerItem,
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    component: () => import("../views/Dashboard.vue"),
    beforeEnter(to, from, next) {
      // reloading producer when coming back from membership page to dashboard to load any changes and reset modal
      // should probably look at not making this request everytime... Could get costly when more users
      // if (store.state.producer) {
      // store.dispatch("getCompanyName", {
      //   companyName: store.state.userInfo.companyName,
      // });
      // }
      // const user = firebase.auth().currentUser;
      firebase.auth().onAuthStateChanged((user) => {
        if (user) {
          next();
        } else {
          next({ name: "login" });
        }
      });
      // console.log(user, "==== USER =====");
      // if (user) {
      //   next();
      // } else {
      //   next({ name: "login" });
      // }
    },
    // TODO: Might need to code split this as above... Benefits:
  },
  {
    path: "/login",
    name: "login",
    component: Login,
  },
  {
    path: "/recipes",
    name: "Recipes",
    component: RecipesList,
  },
  {
    path: "/register",
    name: "register",
    component: Register,
  },
  {
    path: "/privacy-policy",
    name: "privacy policy",
    component: () => import("@/views/privacy-policy.vue"),
  },
  {
    path: "/membership",
    name: "membership",
    component: () => import("@/components/membership/membership.vue"),
  },
  {
    path: "/forgot-password",
    name: "forgotPassword",
    component: ForgotPassword,
    // beforeEnter(to, from, next) {
    // const user = firebase.auth().currentUser;
    // console.log(user);
    // if (user) {
    //   next({ name: "dashboard" });
    // } else {
    //   next();
    // }
    // },
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
