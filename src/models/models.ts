export interface IProducer {
  id: string;
  companyName: string;
  description: string;
  postcode: string;
  type: string[];
  category?: string[];
  county?: string;
  icon?: string;
  lat: string;
  lng: string;
  logo?: string;
  email?: string;
  website?: string;
  facebook?: string;
  twitter?: string;
  instagram?: string;
  membership?: boolean;
  shopOnline?: boolean;
  hasShop?: boolean;
  organic?: boolean;
  image?: string;
  tags?: string[];
  reviews?: string[];
}

export interface IProducerLocation {
  id: string;
  companyName: string;
  description: string;
  icon: {
    url: string;
    size: {
      width: number;
      height: number;
      f: string;
      b: string;
    };
    scaledSize: {
      width: number;
      height: number;
      f: string;
      b: string;
    };
  };
  position: {
    lat: number;
    lng: number;
  };
}

export interface IMarker {
  id: string;
  companyName: string;
  description: string;
  icon: {
    scaledSize: {
      b: string;
      f: string;
      height: number;
      width: number;
    };
    size: {
      b: string;
      f: string;
      height: number;
      width: number;
    };
    url: string;
  };
  position: {
    lat: string | number;
    lng: number;
  };
  lat: string;
  lng: string;
}
