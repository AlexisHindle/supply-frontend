import Vue from "vue";
import Vuex from "vuex";

import { IMarker, IProducer } from "@/models/models";

Vue.use(Vuex);
const endpoint =
  "https://us-central1-squid-inc-8a115.cloudfunctions.net/graphql";
// "http://localhost:5001/squid-inc-8a115/us-central1/graphql";
const initiateState: () => {
  loadingState: boolean;
  alertMessage: boolean;
  markers: IMarker[];
  user: {
    loggedIn: boolean;
    data: null;
  };
  userInfo: unknown;
  image: string;
  location: {
    lat: string;
    lng: string;
  };
  producer: IProducer;
  producers: IProducer[];
  reviews: [];
  recipes: [];
  recipe: unknown;
  itemsToShare: [];
  notifications: [];
  threadIds: [];
  groupedNotifications: [];
  sharedItem: unknown;
} = () => ({
  markers: [],
  alertMessage: false,
  loadingState: false,
  user: {
    loggedIn: false,
    data: null,
  },
  userInfo: {},
  image: "",
  location: {
    lat: "",
    lng: "",
  },
  hasMembership: false,
  producers: [],
  producer: {
    id: "",
    companyName: "",
    description: "",
    postcode: "",
    lat: "",
    lng: "",
    type: [],
  },
  itemsToShare: [],
  reviews: [],
  recipes: [],
  recipe: {},
  notifications: [],
  groupedNotifications: [],
  threadIds: [],
  sharedItem: {},
});
export default new Vuex.Store({
  state: initiateState,
  getters: {
    user: (state) => {
      return state.user;
    },
    loadingState: (state) => state.loadingState,
    getProducer: (state) => state.producer,
    producers: (state) => state.producers,
    getReviews: (state) => state.reviews,
    mapMarkers: (state) => state.markers,
    producerTypes(state): { value: string; text: string }[] {
      const producerTypes = state.producers.map((producer) =>
        producer.type?.map((x) => ({
          value: x,
          text: x[0].toUpperCase() + x.substring(1),
        }))
      );
      return producerTypes.flat();
    },
  },
  mutations: {
    SET_MARKERS(state, value) {
      state.markers = value;
    },
    ALERT_MESSAGE(state, alertMessage) {
      state.alertMessage = alertMessage;
    },
    LOADING_STATE(state, loadingState) {
      state.loadingState = loadingState;
    },
    SET_IMAGE(state, value) {
      state.image = value;
    },
    SET_THREAD_IDS(state, value) {
      state.threadIds = value;
    },
    // SET_GROUPED_NOTIFICATIONS(state, value) {
    //   console.log(value);
    //   // state.groupedNotifications.push(value);
    //   state.groupedNotifications = value;
    // },
    SET_NOTIFICATIONS(state, value) {
      state.notifications = value;
    },
    SET_LOCATION(state, value) {
      state.location = value;
    },
    SET_LOGGED_IN(state, value) {
      state.user.loggedIn = value;
    },
    SET_USER(state, data) {
      state.user.data = data;
    },
    SET_USER_INFO(state, data) {
      state.userInfo = data;
    },
    SET_ITEMS_TO_SHARE(state, value): void {
      state.itemsToShare = value;
    },
    SET_PRODUCERS(state, producers): void {
      state.producers = producers;
    },
    SET_PRODUCER(state, producer): void {
      state.producer = producer;
    },
    SET_REVIEWS(state, reviews): void {
      state.reviews = reviews.reviews;
    },
    SET_RECIPES(state, data) {
      state.recipes = data;
    },
    SET_RECIPE(state, data) {
      state.recipe = data;
    },
    SET_SHARED_ITEM(state, data) {
      state.sharedItem = data;
    },
  },

  // create company has id
  // create user pass companyID
  actions: {
    fetchUser({ commit }, user) {
      commit("SET_LOGGED_IN", user !== null);
      if (user) {
        commit("SET_USER", {
          displayName: user.name,
          email: user.email,
        });
      } else {
        commit("SET_USER", null);
      }
    },
    async getItemsToShare({ commit }): Promise<void> {
      const response = await fetch(endpoint, {
        method: "POST",
        headers: { "content-type": "application/json" },
        body: JSON.stringify({
          query: `{
            itemsToShare {
              id
              itemName
              description
              userId
              username
              dateAdded
              quantity
              pickUpInformation
              image
              lat
              lng
              useByDate
              address
              type
              user {
                firstName
                lastName
                id
                userType
                company {
                  companyName
                  description
                }
              }
            }
          }`,
        }),
      });
      const responseBody = await response.json();
      const { itemsToShare } = responseBody.data;
      commit("SET_ITEMS_TO_SHARE", itemsToShare);
      return itemsToShare;
    },
    async createItemToShare({ commit }, input): Promise<void> {
      try {
        const response = await fetch(endpoint, {
          method: "POST",
          headers: { "content-type": "application/json" },
          body: JSON.stringify({
            query: `mutation Mutation($input: createItemToShareInput!) {
              sharedItem: createItemToShare(input: $input) {
                username
                type
                itemName
                quantity
                pickUpInformation
                useByDate
                dateAdded
                description
                userId
                image
                lat
                lng
                address
              }
            }`,
            variables: {
              input: {
                userId: input.userId,
                itemName: input.itemName,
                description: input.description,
                username: input.username,
                useByDate: input.useByDate,
                image: input.image,
                pickUpInformation: input.pickUpInformation,
                lat: input.lat,
                lng: input.lng,
                dateAdded: input.dateAdded,
                quantity: input.quantity,
                address: input.address,
              },
            },
          }),
        });
        const responseBody = await response.json();
        const sharedItem = responseBody.data;
        commit("SET_SHARED_ITEM", sharedItem);
        return sharedItem;
      } catch (error) {
        console.error(error);
      }
    },
    async updateItemToShare({ commit }, { id, input }): Promise<void> {
      try {
        const response = await fetch(endpoint, {
          method: "POST",
          headers: { "content-type": "application/json" },
          body: JSON.stringify({
            query: `mutation UpdateItemToShare($id: ID!, $input: createItemToShareInput!) {
              itemToShare: updateItemToShare(id: $id, input: $input) {
                itemName
                type
                dateAdded
                pickUpInformation
                description
                image
                quantity
                image
                lat
                lng
                useByDate
              }
            }`,
            variables: {
              id: id,
              input: {
                itemName: input.itemName,
                useByDate: input.luseByDate,
                image: input.image,
                pickUpInformation: input.pickUpInformation,
                lat: input.lat,
                lng: input.lng,
                dateAdded: input.dateAdded,
                userType: input.userType,
                quantity: input.quantity,
                username: input.username,
                type: input.type,
              },
            },
          }),
        });
        const responseBody = await response.json();
        const { itemToShare } = responseBody.data;
        commit("SET_SHARED_ITEM", itemToShare);
        return itemToShare;
      } catch (err) {
        console.error(err);
      }
    },
    async getMessagesByUserId({ commit }, id): Promise<void> {
      const response = await fetch(endpoint, {
        method: "POST",
        headers: { "content-type": "application/json" },
        body: JSON.stringify({
          query: `query Query($userId: ID!) {
            messagesByUserId(userId: $userId) {
              id
              messages {
                read
                threadId
                message
                itemName
                sender {
                  firstName
                  lastName
                  id
                }
                created
              }
              users {
                firstName
                lastName
                id
                readMessages
              }
            }
          }`,
          variables: {
            userId: id,
          },
        }),
      });
      const responseBody = await response.json();
      const { messagesByUserId } = responseBody.data;
      console.log(messagesByUserId);
      commit("SET_NOTIFICATIONS", messagesByUserId);
      return messagesByUserId;
    },
    // async getMessagesByThreadId({ commit }, threadId): Promise<void> {
    //   const response = await fetch(endpoint, {
    //     method: "POST",
    //     headers: { "content-type": "application/json" },
    //     body: JSON.stringify({
    //       query: `query Messages($threadId: ID!) {
    //         messagesByThreadId(threadId: $threadId) {
    //           id
    //           sentById
    //           sentBy
    //           receivedBy
    //           receivedById
    //           dateSent
    //           itemId
    //           itemName
    //           message
    //           read
    //           threadId
    //         }
    //       }`,
    //       variables: {
    //         threadId: threadId,
    //       },
    //     }),
    //   });
    //   const responseBody = await response.json();
    //   const { messagesByThreadId } = responseBody.data;
    //   console.log(messagesByThreadId);
    //   commit("SET_GROUPED_NOTIFICATIONS", messagesByThreadId);
    //   return messagesByThreadId;
    // },
    async sendMessage({ commit }, input): Promise<void> {
      try {
        const response = await fetch(endpoint, {
          method: "POST",
          headers: { "content-type": "application/json" },
          body: JSON.stringify({
            query: `
            mutation SendItemRequest($input: sendItemRequestInput!) {
              sendItemRequest(input: $input) {
                users {
                  firstName
                  lastName
                  id
                  readMessages
                }
                messages {
                  threadId
                  message
                  itemName
                  read
                  sender {
                    firstName
                    lastName
                    id
                  }
                  created
                }
              }
            }`,
            variables: {
              input: {
                created: input.created,
                users: input.users,
                messages: input.messages,
              },
            },
          }),
        });
        const responseBody = await response.json();
        const { sendItemRequest } = responseBody.data;
        if (sendItemRequest) {
          commit("ALERT_MESSAGE", true);
        }
        return sendItemRequest;
      } catch (error) {
        console.error(error);
      }
    },
    async updateMessage({ commit }, { id, input }): Promise<void> {
      try {
        const response = await fetch(endpoint, {
          method: "POST",
          headers: { "content-type": "application/json" },
          body: JSON.stringify({
            query: `mutation Mutation($id: ID!, $input: sendItemRequestInput!) {
              updateItemRequest(id: $id, input: $input) {
                users {
                  firstName
                  lastName
                  id
                }
                messages {
                  threadId
                  itemName
                  message
                  read
                  sender {
                    id
                    lastName
                    firstName
                    readMessages
                  }
                  created
                }
              }
            }`,
            variables: {
              id: id,
              input: {
                messages: input.messages,
              },
            },
          }),
        });
        const responseBody = await response.json();
        const { updateItemRequest } = responseBody.data;
        if (updateItemRequest) {
          commit("ALERT_MESSAGE", true);
        }
        return updateItemRequest;
      } catch (error) {
        console.error(error);
      }
    },
    // async messageRepy({ commit }, { input }): Promise<void> {},
    async deleteItemToShare({ commit }, id): Promise<void> {
      try {
        const response = await fetch(endpoint, {
          method: "POST",
          headers: { "content-type": "application/json" },
          body: JSON.stringify({
            query: `mutation Mutation($id: ID!) {
              deletedItem: deleteItemToShare(id: $id)
            }`,
            variables: {
              id: id,
            },
          }),
        });
        const responseBody = await response.json();
        const { deletedItem } = responseBody.data;
        // TODO: Need to look if this is working
        commit("SET_SHARED_ITEM", deletedItem);
        return deletedItem;
      } catch (error) {
        console.error(error);
      }
    },
    async getUser({ commit }, email): Promise<void> {
      try {
        const response = await fetch(endpoint, {
          method: "POST",
          headers: { "content-type": "application/json" },
          body: JSON.stringify({
            query: `query User($email: String!) {
              user(email: $email) {
                id
                firstName
                lastName
                email
                userType
                companyName
                company {
                  id
                  companyName
                  description
                }
                itemsToShare {
                  id
                  itemName
                  dateAdded
                  username
                  userId
                  quantity
                  image
                  lat
                  lng
                  useByDate
                }
              }
            }`,
            variables: {
              email: email,
            },
          }),
        });
        const responseBody = await response.json();
        const { user } = responseBody.data;
        commit("SET_USER_INFO", user);
        // if (user) {
        //   commit("LOADING_STATE", false);
        // }
        return user;
      } catch (error) {
        console.error(error);
      }
    },
    async createUser({ commit }, input): Promise<void> {
      try {
        const response = await fetch(endpoint, {
          method: "POST",
          headers: { "content-type": "application/json" },
          body: JSON.stringify({
            query: `mutation Mutation($input: createUserInput!) {
              createUser(input: $input) {
                firstName
                lastName
                companyName
                email
                userType
                userUid
              }
            }`,
            variables: {
              input: input,
            },
          }),
        });
        const responseBody = await response.json();
        const { user } = responseBody.data;
        commit("SET_USER_INFO", user);
        // if (user) {
        //   commit("LOADING_STATE", false);
        // }
        return user;
      } catch (error) {
        console.error(error);
      }
    },
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async deleteUserProfile({ commit }, id): Promise<void> {
      try {
        const response = await fetch(endpoint, {
          method: "POST",
          headers: { "content-type": "application/json" },
          body: JSON.stringify({
            query: `mutation DeleteUser($deleteUserId: ID!) {
              deleteUser(id: $deleteUserId)
            }`,
            variables: {
              id: id,
            },
          }),
        });
        const responseBody = await response.json();
        const { deletedItem } = responseBody.data;
        return deletedItem;
      } catch (error) {
        console.error(error);
      }
    },
    async updateUser(
      { commit },
      { firstName, lastName, email, id, userType }
    ): Promise<void> {
      const response = await fetch(endpoint, {
        method: "POST",
        headers: { "content-type": "application/json" },
        body: JSON.stringify({
          query: `mutation UpdateUser($id: ID!, $input: createUserInput!) {
            updateUser(id: $id, input: $input) {
              firstName
              lastName
              email
              userType
            }
          }`,
          variables: {
            id: id,
            input: {
              firstName: firstName,
              lastName: lastName,
              email: email,
              userType: userType,
            },
          },
        }),
      });
      const responseBody = await response.json();
      const { updatedUser } = responseBody.data;
      commit("SET_USER_INFO", updatedUser);
      return updatedUser;
    },
    async createCompany({ commit }, input): Promise<void> {
      try {
        const response = await fetch(endpoint, {
          method: "POST",
          headers: { "content-type": "application/json" },
          body: JSON.stringify({
            query: `mutation CreateCompany($input: createCompanyInput!) {
              createCompany(input: $input) {
                companyName
                description
                postcode
                type
                category
                county
                icon
                lat
                lng
                logo
                email
                website
                facebook
                twitter
                instagram
                membership
                shopOnline
                hasShop
                organic
                image
                tags
              }
            }`,
            variables: {
              input: {
                companyName: input.companyName,
                description: input.description,
                postcode: input.postcode,
                county: input.county,
                type: input.type,
                category: input.category,
                lat: input.lat,
                lng: input.lng,
                organic: input.organic,
                hasShop: input.hasShop,
                tags: input.tags,
                facebook: input.facebook,
                twitter: input.twitter,
                instagram: input.instagram,
                email: input.email,
                website: input.website,
              },
            },
          }),
        });
        const responseBody = await response.json();
        const { company } = responseBody.data;
        commit("SET_PRODUCER", company);
        return company;
      } catch (error) {
        console.error(error);
      }
    },
    async updateCompany(
      { commit },
      {
        id,
        companyName,
        description,
        postcode,
        county,
        type,
        category,
        lat,
        lng,
        organic,
        hasShop,
        tags,
        facebook,
        twitter,
        instagram,
        email,
        website,
        image,
        membership,
      }
    ): Promise<void> {
      const response = await fetch(endpoint, {
        method: "POST",
        headers: { "content-type": "application/json" },
        body: JSON.stringify({
          query: `mutation UpdateCompany($id: ID!, $input: createCompanyInput!) {
            updateCompany(id: $id, input: $input) {
              companyName
              description
              postcode
              type
              category
              county
              icon
              lat
              lng
              logo
              email
              website
              facebook
              twitter
              instagram
              membership
              shopOnline
              hasShop
              organic
              image
              tags
            }
          }`,
          variables: {
            id: id,
            input: {
              companyName: companyName,
              description: description,
              postcode: postcode,
              county: county,
              type: type,
              category: category,
              lat: lat,
              lng: lng,
              organic: organic,
              hasShop: hasShop,
              tags: tags,
              facebook: facebook,
              twitter: twitter,
              instagram: instagram,
              email: email,
              website: website,
              image: image,
              membership: membership,
            },
          },
        }),
      });
      const responseBody = await response.json();
      const { updatedProducer } = responseBody.data;
      commit("SET_PRODUCER", updatedProducer);
      return updatedProducer;
    },
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async deleteCompany({ commit }, id): Promise<void> {
      try {
        const response = await fetch(endpoint, {
          method: "POST",
          headers: { "content-type": "application/json" },
          body: JSON.stringify({
            query: `mutation DeleteCompany($id: ID!) {
              deletedCompany: deleteCompany(id: $id)
            }`,
            variables: {
              id: id,
            },
          }),
        });
        const responseBody = await response.json();
        const { deletedCompany } = responseBody.data;
        return deletedCompany;
      } catch (error) {
        console.error(error);
      }
    },
    async getCompanies({ commit, dispatch }): Promise<void> {
      commit("LOADING_STATE", true);
      const response = await fetch(endpoint, {
        method: "POST",
        headers: { "content-type": "application/json" },
        body: JSON.stringify({
          query: `{
            companies {
              id
              companyName
              description
              postcode
              type
              category
              county
              icon
              lat
              lng
              logo
              email
              website
              facebook
              twitter
              instagram
              membership
              shopOnline
              hasShop
              organic
              image
              tags
              reviews {
                companyName
                rating
                author
                review
                date
                id
              }
            }
          }`,
        }),
      });
      const responseBody = await response.json();
      const { companies } = responseBody.data;
      commit("SET_PRODUCERS", companies);
      console.log(companies);
      dispatch("addMarkers", companies);
      // if (companies.length) {
      //   commit("LOADING_STATE", false);
      // }
      return companies;
    },
    async getCompanyName({ commit }, { companyName }): Promise<void> {
      if (companyName) {
        try {
          const response = await fetch(endpoint, {
            method: "POST",
            headers: { "content-type": "application/json" },
            body: JSON.stringify({
              query: `query FindCompanyByName($companyName: String) {
                  findCompanyByName(companyName: $companyName) {
                    companyName
                    description
                    postcode
                    type
                    category
                    county
                    id
                    icon
                    lat
                    lng
                    tags
                    logo
                    email
                    website
                    facebook
                    twitter
                    instagram
                    membership
                    shopOnline
                    hasShop
                    organic
                    image
                  }
                }`,
              variables: {
                companyName: companyName,
              },
            }),
          });

          const responseBody = await response.json();

          if (companyName) {
            commit("SET_PRODUCER", responseBody.data.findCompanyByName);
          }
          return responseBody.data;
        } catch (error) {
          console.error(error);
        }
      }
    },
    async getCompanyById({ commit }, { id }): Promise<void> {
      try {
        const response = await fetch(endpoint, {
          method: "POST",
          headers: { "content-type": "application/json" },
          body: JSON.stringify({
            query: `query Company($id: ID!) {
              company(id: $id) {
                companyName
                id
                description
                postcode
                type
                category
                county
                icon
                lat
                lng
                logo
                email
                website
                facebook
                twitter
                instagram
                membership
                shopOnline
                hasShop
                organic
                image
                tags
                reviews {
                  id
                  companyName
                  rating
                  author
                  review
                  date
                }
              }
            }`,
            variables: {
              id: id,
            },
          }),
        });
        const responseBody = await response.json();
        const { company } = responseBody.data;
        commit("SET_PRODUCER", company);
        return company;
      } catch (error) {
        console.error(error);
      }
    },
    async getReviews({ commit }): Promise<void> {
      const response = await fetch(endpoint, {
        method: "POST",
        headers: { "content-type": "application/json" },
        body: JSON.stringify({
          query: `{
            reviews {
              companyName
              author
              rating
              review
              date
            }
          }`,
        }),
      });
      const responseBody = await response.json();
      commit("SET_REVIEWS", responseBody.data);
      return responseBody.data;
    },
    async getRecipes({ commit }, category): Promise<void> {
      const response = await fetch(
        `https://www.themealdb.com/api/json/v2/9973533/search.php?s=${category}`,
        {
          method: "GET",
        }
      );
      const responseBody = await response.json();

      const recipes = responseBody.meals.map((meal) => {
        // for each recipe we need to create and array of ingredients
        return {
          recipe: meal.strMeal,
          instructions: meal.strInstructions,
          measures: [...Array(20)]
            .map((_, index) => meal[`strMeasure${index}`])
            .filter((m) => m),
          ingredients: [...Array(20)]
            .map((_, index) => meal[`strIngredient${index}`])
            .filter((m) => m),
        };
      });
      commit("SET_RECIPES", recipes);
      // return responseBody.meals;
    },
    addMarkers({ commit }, array: IMarker[]): void {
      const markers: IMarker[] = [];
      array.forEach((element) => {
        const marker: IMarker = {
          id: element.id,
          companyName: element.companyName,
          description: element.description,
          icon: {
            url: element.icon
              ? require(`../../public/img/icon-pins/${element.icon}.svg`)
              : require(`../../public/img/pin-1.png`),
            size: { width: 40, height: 60, f: "px", b: "px" },
            scaledSize: { width: 40, height: 60, f: "px", b: "px" },
          },
          position: {
            lat: parseFloat(element.lat),
            lng: parseFloat(element.lng),
          },
          lat: element.lat,
          lng: element.lng,
        };
        markers.push(marker);
      });
      commit("SET_MARKERS", markers);
      if (markers.length) {
        commit("LOADING_STATE", false);
      }
    },
  },
  modules: {},

  // mdiFoodDrumstickOutline meat
  // mdiFoodAppleOutline fruit
  // mdiFoodCroissant baked
  // mdiFoodVariant dairy
  // mdi-shaker-outline cupboard/larder TODO: need something better
});
