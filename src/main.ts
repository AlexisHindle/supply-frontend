import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import * as VueGoogleMaps from "vue2-google-maps";
import Geocoder from "@pderas/vue2-geocoder";
import * as firebase from "firebase";
import Meta from "vue-meta";

Vue.config.productionTip = false;

const firebaseConfig = {
  apiKey: "AIzaSyD40spY72-eXJMOJda8LUxz1wiVS3Za0WQ",
  authDomain: "squid-inc-8a115.firebaseapp.com",
  databaseURL: "https://squid-inc-8a115.firebaseio.com",
  projectId: "squid-inc-8a115",
  storageBucket: "squid-inc-8a115.appspot.com",
  messagingSenderId: "689136088904",
  appId: "1:689136088904:web:e12a903980ad1f2b4b257d",
  measurementId: "G-XE65P3P1C5",
};

firebase.initializeApp(firebaseConfig);
const db = firebase.database();

export { db };

Vue.use(Meta, { keyName: "metaInfo" });

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyDg0qe-o5o6Nz4i3sMeLhyjXxisJd1CBiI",
    libraries: "places, geometry",
    installComponents: true,
  },
});

Vue.use(Geocoder, {
  defaultCountryCode: "UK", // e.g. 'CA'
  defaultLanguage: "en", // e.g. 'en'
  defaultMode: "lat-lng", // or 'address'
  googleMapsApiKey: "AIzaSyDg0qe-o5o6Nz4i3sMeLhyjXxisJd1CBiI",
});

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
