import Vuex from "vuex";
import Vuetify from "vuetify";
import { shallowMount } from "@vue/test-utils";
import Home from "@/views/Home.vue";
import Vue from "vue";
import FilterComponent from "@/components/filters/filters.vue";
import ProducerListContainer from "@/components/producers-list/producers-list.vue";

const mockGeolocation = {
  getCurrentPosition: jest.fn(),
  watchPosition: jest.fn(),
};

Vue.use(Vuex);
Vue.use(Vuetify);

describe("Home component", () => {
  let store;
  // let propsData;
  beforeEach(() => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (window as any).navigator.geolocation = mockGeolocation;
    store = new Vuex.Store({
      state: {
        markers: {
          id: "0fjTp82aHe0jfgGbRyjK",
          companyName: "Test marker",
          description: "Test marker description",
          postcode: "RG23 8PU",
          type: ["shop"],
          lat: "51.262054",
          lng: "-1.148443",
        },
        loadingState: false,
        producers: [
          {
            id: "0fjTp82aHe0jfgGbRyjK",
            companyName: "Test producer",
            description: "Test description",
            postcode: "RG23 8PU",
            type: ["shop"],
            category: [],
            county: "",
            icon: "",
            lat: "51.262054",
            lng: "-1.148443",
            logo: "",
            email: "",
            website: "",
            facebook: "",
            twitter: "",
            instagram: "",
            membership: false,
            shopOnline: "",
            hasShop: "",
            organic: "",
            image: "",
            tags: "",
            reviews: [],
          },
        ],
      },
      getters: {
        loadingState: () => store.state.loadingState,
        producers: () => store.state.producers,
        mapMarkers: () => store.state.markers,
        producerTypes: () => [{ value: "shop", text: "Shop" }],
      },
    });
    // propsData = {};
  });

  // afterEach(() => {
  //   propsData = {};
  // });

  describe("component rendering", () => {
    it("should mount without error", () => {
      shallowMount(Home, { store });
    });

    describe("Should render child components", () => {
      it("should render the filter component with no errors", () => {
        const wrapper = shallowMount(Home, { store });
        const component = wrapper.findComponent(FilterComponent);
        expect(component.exists()).toBeTruthy();
        expect(component.props()).toMatchObject({
          usePostcode: true,
          types: [{ value: "shop", text: "Shop" }],
          selected: false,
          categories: [],
        });
      });
      it("should NOT render the filter component", () => {
        store.state.loadingState = true;
        const wrapper = shallowMount(Home, { store });
        const component = wrapper.findComponent(FilterComponent);
        expect(component.exists()).toBeFalsy();
      });
      it("should render the producer list component with no errors", () => {
        const wrapper = shallowMount(Home, { store });
        const component = wrapper.findComponent(ProducerListContainer);
        expect(component.exists()).toBeTruthy();
        expect(component.props()).toMatchObject({
          producer: {
            id: "0fjTp82aHe0jfgGbRyjK",
            companyName: "Test producer",
            description: "Test description",
            postcode: "RG23 8PU",
            type: ["shop"],
            category: [],
            county: "",
            icon: "",
            lat: "51.262054",
            lng: "-1.148443",
            logo: "",
            email: "",
            website: "",
            facebook: "",
            twitter: "",
            instagram: "",
            membership: false,
            shopOnline: "",
            hasShop: "",
            organic: "",
            image: "",
            tags: "",
            reviews: [],
          },
          size: undefined,
        });
      });
      it("should NOT render the producer list component", () => {
        store.state.loadingState = true;
        const wrapper = shallowMount(Home, { store });
        const component = wrapper.findComponent(ProducerListContainer);
        expect(component.exists()).toBeFalsy();
      });
    });
  });
});
